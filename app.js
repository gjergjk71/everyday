const graphqlServer = require("graphql-yoga").GraphQLServer;
const resolvers = require("./resolvers");
const jwt = require("jsonwebtoken");
const configs = require("./configs");
const prismaTypeDefs = require("./generated/prisma-client/prisma-schema.js").typeDefs;
const { Prisma } = require("prisma-binding");
const cors = require("cors");
const { formatError } = require("apollo-errors");

const prisma = new Prisma({
	typeDefs: prismaTypeDefs,
	endpoint: "https://eu1.prisma.sh/gjergj-kadriu-c6f550/everyday/dev",
	debug: false
})

const options =  {
  formatError
};

const server = new graphqlServer({
	typeDefs: "./schema.graphql", 
	resolvers,
	formatError: err => {
		console.log(err,5);
	},
	context: async req => {
		var user = undefined
		if (req.request.headers["authorization"]){
			const token = req.request.headers["authorization"].split(" ")[1];
			const decoded = await jwt.verify(token,configs.jwt_secret,(err,decoded) => {
				if (err) {
					if (err.name === "JsonWebTokenError") {
						console.log("JsonWebTokenError");
						return Error("Invalid token");
					}
					return new Error("Invalid token");
				}
				return decoded
			});
			if (!decoded.message){
				user = await prisma.query.user({where:{id:decoded.userId}});
			}
		}
		if (user) user.password = null;
		return {
			req,
			user,
			db:prisma
		}
	}
});

server.express.use(cors());
server.start(options,() => console.log("Running on port 4000"));