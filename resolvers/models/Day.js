
const permissions = require("../permissions");

const days = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	if (!args.item){
		throw new Error("item arg required");
	}
	let item = await context.db.query.items({where:{
		id:args.item,
		createdBy: {
			id: context.user.id
		}
	}})
	if (!item.length){
		throw new Error("item not found");
	}
	return await context.db.query.days({where:{item:{id:args.item}}});
}

const createOrDeleteDay = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	if (!args.date || !args.item){
		throw new Error("date/item argument is required");
	}
	let item = await context.db.query.items({
		where: {
			id: args.item,
			createdBy: { id: context.user.id }
		}
	})
	if (!item.length){
		throw new Error(`Item with id ${args.item}not found`);
	}
	item = item[0]
	let day = await context.db.query.days({
		where:{
			item: { id: item.id },
			date: new Date(args.date)
		}
	},`
		{
			id
			date
			item {
				id
				createdBy {
					id
				}
			}
		}
	`
	);
	console.log(day.length);
	day = day[0];
	console.log(day);
	if (day){
		await context.db.mutation.deleteDay({
			where:{id:day.id}
		})
		return {
			id: day.id,
			date: new Date(args.date)	,
			action: "DELETED"
		}
	}
	day = await context.db.mutation.createDay({
		data:{
			date:new Date(args.date),
			item:{
				connect:{
					id:args.item
				}
			}
		}
	})
	return {
		id: day.id,
		date: args.date,
		action: "CREATED"
	}


}

module.exports = {
	days,
	createOrDeleteDay: createOrDeleteDay
}