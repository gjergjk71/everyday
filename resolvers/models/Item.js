const permissions = require("../permissions");

const items = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	return await context.db.query.items({
		where: { createdBy: {id: context.user.id }},
		orderBy: "createdAt_DESC"
	},info)
}

const resetItem = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	let items = await context.db.query.items({
		where:{
			id: args.item,
			createdBy: { id: context.user.id }
		}
	},info)	
	if (!items.length) {
		throw new Error("Item not found");
	}
	await context.db.mutation.deleteManyDays({
		where:{
			item: { id: args.item }
		}
	})
	let item = items[0];
	item.days = []
	return item
}

const createItem = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	if (!args.name){
		throw new Error("name argument is required");
	}
	args["createdBy"] = {connect:{id:context.user.id}};
	return context.db.mutation.createItem({data:args},info);
}

const updateItem = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	const item = await context.db.query.item({where:{id:args.id}},`
		{
			createdBy {
				id
			}
		}
	`);
	if (!item){
		throw new Error("Item not found");
	}
	if (item.createdBy.id !== context.user.id) {
		throw new Error("Unauthorized");
	}
	const obj = await context.db.mutation.updateItem({
		where:{id:args.id},
		data: {
			name: args.name
		}
	})
	return obj;
}

const deleteItem = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	const item = await context.db.query.item({where:{id:args.id}},`
		{
			createdBy {
				id
			}
		}
	`);
	if (!item){
		throw new Error("Item not found");
	}
	if (item.createdBy.id !== context.user.id) {
		throw new Error("Unauthorized");
	}
	const obj = await context.db.mutation.deleteItem({where:{id:args.id}},info);
	return obj;
}

module.exports = {
	items,
	createItem,
	deleteItem,
	updateItem,
	resetItem
}