const bcrypt = require("bcrypt");
const permissions = require("../permissions");
const saltRounds = 10;
const { CreateValidationError, checkValidation } = require("../../helpers.js");
const { updateUserSchema } = require("../../validationSchemas.js");

const getLoggedInUser = async (parent,args,context,info) => {
	if (!context.user){
		return undefined
	}
	return await context.db.query.user({where:{id:context.user.id}},info);
}

const updateUser = async (parent,args,context,info) => {
	permissions.loginPermissions(context);
	let errors = await checkValidation(updateUserSchema, args,false);
	console.log(errors.errors,5);
	const data = {}
	var direct_fields = ["first_name","last_name","email"]
	for (var x in direct_fields){
		if (args[direct_fields[x]] && args[direct_fields[x]].length){
			data[direct_fields[x]] = args[direct_fields[x]];
		}
	}
	if (args.password) {
		user = await context.db.query.user({where:{id:context.user.id}});
		validOldPassword = await bcrypt.compare(args.password.old_password,user.password);
		if (!validOldPassword){
			errors.length ? errors.push("old_password is incorrect") : errors = ["old_password is incorrect"]
		}
		data["password"] = await bcrypt.hash(args.password.new_password,saltRounds);
	}
	if (errors.length){
		let ValidationError = CreateValidationError(errors);
		throw new ValidationError
	}
	return await context.db.mutation.updateUser({
		data,where:{id:context.user.id}
	},info)
}

module.exports = {
	getLoggedInUser,
	updateUser
}