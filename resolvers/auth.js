const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const configs = require("../configs");
const { checkValidation, CreateValidationError } = require("../helpers.js");
const { loginSchema, registerSchema } = require("../validationSchemas.js");

const saltRounds = 10;

const createToken = (userId) => {
	const token = jwt.sign({
		userId: userId
	},configs.jwt_secret);
	return token
}

const login = async (root,args,context,info) => {
	let errors = await checkValidation(loginSchema,args,false)
	let ERROR = CreateValidationError(["Your Email or Password is incorrect"])
	if (errors.length){
		throw new ERROR;
	}
	const user = await context.db.query.user({where:{email:args.email}});
	if (!user){
		throw new ERROR;
	}
	const validPassword = await bcrypt.compare(args.password,user.password);
	if (!validPassword){
		throw new ERROR;	
	}
	return {
		userId: user.id,
		token: createToken(user.id),
		expiresIn: 1	
	};
}

const signUp = async (root,args,context) => {
	var errors = await checkValidation(registerSchema, args,false)
	const hashed_password = await bcrypt.hash(args.password,saltRounds);
	var userParams = {
		first_name: args.first_name,
		last_name: args.last_name,
		email: args.email,
		password: hashed_password
	}
	let invalid_email = errors.length && errors.indexOf("email must be a valid email") !== -1
	if (errors.length && !invalid_email){
		var exists = await context.db.query.users({where:{email:args.email}})
		if (exists.length){
			errors.push("Email is already taken");
		}
	} else if (!invalid_email) {
		try {
			var user = await context.db.mutation.createUser({data:userParams});
		} catch (e) {
			if (e.message === "A unique constraint would be violated on User. Details: Field name = email"){
				var errors = ["Email is already taken."]
			}
		}
	}
	if (errors.length){
		let ERROR = CreateValidationError(errors);
		throw new ERROR;
	}
	return {
		userId: user.id,
		token: createToken(user.id),
		expiresIn: 1
	};
}

module.exports = {
	login,
	signUp
}