const authResolvers = require("./auth");
const userResolvers = require("./models/User");
const itemResolvers = require("./models/Item");
const dayResolvers = require("./models/Day");

module.exports = {
	Query: {
		items: itemResolvers.items,
		days: dayResolvers.days,
		getLoggedInUser: userResolvers.getLoggedInUser
	},
	Mutation: {
		login: authResolvers.login,
		signUp: authResolvers.signUp,
		updateUser: userResolvers.updateUser,
		resetItem: itemResolvers.resetItem,
		createItem: itemResolvers.createItem,
		deleteItem: itemResolvers.deleteItem,
		updateItem: itemResolvers.updateItem,
		createOrDeleteDay: dayResolvers.createOrDeleteDay
	}
}